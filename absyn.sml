structure Absyn =
struct

datatype BinOp = Plus | Minus | Times | Divide;

datatype LogOp = And | Or | Ge | Gt | Lt | Le | Eq

datatype Expr  = Const of int
	       | Str of string
	       | ID_abs of string
	       | Op    of Expr * BinOp * Expr
	       | Cop     of Expr * LogOp * Expr 

	
type Params = (Expr list)
type Params_decl = string list

	       
datatype Statm = ifelse of (Statm list) * Expr * (Statm list)
		| loop  of Expr * (Statm list)
		| assign of string * Expr
		| string_assign of string * Expr
		| decl   of string * Expr 
		| ab_exp of Expr
		| ifstat of (Statm list) * Expr
		| Break
		| Continue
		| Return of Expr
		| Print of Expr
		| forloop of Statm * Expr* Statm * (Statm list)
		| function_decl of string * Params_decl * (Statm list)
		| function_call of string * string * Params
		| string_decl of string * Expr



		
(* Some helper functions *)
fun plus  a b = Op (a, Plus, b)
fun minus a b = Op (a, Minus, b)
fun times   a b = Op (a, Times, b)
fun divide a b = Op (a, Divide , b)   

fun and_op a b = Cop(a,And,b)
fun or_op  a b = Cop(a,Or,b)
fun gt a b     = Cop(a,Gt,b)
fun ge a b     = Cop(a,Ge,b)
fun lt a b     = Cop(a,Lt,b)
fun le a b     = Cop(a,Le,b)
fun eq a b     = Cop(a,Eq,b)

end


