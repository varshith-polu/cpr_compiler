(*
This file is to generate the javascript intermediate code.
It basically generates code by taking one statement at a time from the list of statements that come from the abstract syntax

*)
structure Code_gen =
struct

(*
The intended purpose of this structure is to store variables which are declared in the scope.
*)
structure MapT = RedBlackMapFn (struct type ord_key = string
		val compare = String.compare
		end);

val global_map = MapT.empty

exception scope_error
exception Type_Error
exception Assign_error

fun give_expr_type(Absyn.Str(s),0) = true
  | give_expr_type(Absyn.Str(s),1) = false
	| give_expr_type(Absyn.Const(x),access) = true
  | give_expr_type(Absyn.ID_abs(x),access) = true
 	| give_expr_type(Absyn.Op(e1,b,e2),access) = give_expr_type(e1,1) andalso give_expr_type(e2,1)
	| give_expr_type(Absyn.Cop(e1,b,e2),access) = give_expr_type(e1,1) andalso give_expr_type(e2,1)

fun give_exp_type(Absyn.Str(s),scope_map) = 0
	| give_exp_type(Absyn.Const(x),scope_map) = 1
	| give_exp_type(Absyn.ID_abs(x),scope_map) = if MapT.find(scope_map,x) = SOME(1) then 1 else 0
	| give_exp_type(Absyn.Op(e1,b,e2),scope_map) = 1
	| give_exp_type(Absyn.Cop(e1,b,e2),scope_map) = 1

(*
expr to string is a function which takes any expression in our grammar and converts to the corresponding code in the javascript
since lot of binary operators are almost same in javascript it just involves translating from one language to other
*)
fun expr_to_string(Absyn.Const(y),scope_map) = (Int.toString(y))
  |  expr_to_string(Absyn.ID_abs(y),scope_map) =  if MapT.find(scope_map,y) = NONE then raise scope_error else (y)
  |  expr_to_string(Absyn.Op(e1,binop,e2),scope_map) = let val s1 = expr_to_string(e1,scope_map)
                                                 val s2 = expr_to_string(e2,scope_map)
                                                 val bin = if binop = Absyn.Plus then " + "
                                                           else
                                                              if binop = Absyn.Minus then " - "
                                                              else
                                                                  if binop = Absyn.Times then " * "
                                                                  else
                                                                    " / " in
                                                  String.concat([s1,bin,s2])
                                              end
  | expr_to_string(Absyn.Cop(e1,binop,e2),scope_map) = let val s1 = expr_to_string(e1,scope_map)
                                                 val s2 = expr_to_string(e2,scope_map)
                                                 val cop = if binop = Absyn.And then " && "
                                                           else
                                                              if binop = Absyn.Or then " || "
                                                              else
                                                                  if binop = Absyn.Ge then " >= "
                                                                  else
                                                                    if binop = Absyn.Gt then " > "
                                                                    else
                                                                      if binop = Absyn.Le then " <= "
                                                                      else
                                                                        if binop = Absyn.Lt then " < "
                                                                        else
                                                                          " == " in
                                                  String.concat([s1,cop,s2])
                                              end
(*
useful to convert multiple expressions of our grammar (a list of expressions) into a string
*)
fun exprs_to_string([],s,scope_map) = ""
  | exprs_to_string(expr::[],s,scope_map) = let val e = expr_to_string(expr,scope_map) in
                                          if s = "" then e else String.concat([e," , ",s])
                                  end

  | exprs_to_string(expr::exprs,s,scope_map) = let val e = expr_to_string(expr,scope_map)
                                         val s_new = exprs_to_string(exprs,s,scope_map) in
                                         String.concat([e,",",s_new])
                                     end


fun insert_list_in_map([],map) = map
  |insert_list_in_map(li::rest,map) = let val new_map = MapT.insert(map,li,1) in
                                           insert_list_in_map(rest,new_map)
                                     end
(*
These is a function which converts a list of statement into javascript code using the below function
These two are basically mutually recursive functions.
*)


fun convert_statements([],s,scope_map) = (s,scope_map)
  | convert_statements(statm::statms,s,scope_map) = let val (s1,new_scope) = convert_statement(statm,scope_map)
                                              val s_new = String.concat([s,s1]) in
                                              convert_statements(statms,s_new,new_scope)
                                          end

(*
A statement is the basic element in our grammar so the approach is to take a statement at a time and recursively
convert it into javascript code
*)

and convert_statement(Absyn.decl(x,y),scope_map) = let val new_scope = MapT.insert(scope_map,x,1)
																													 val check = give_expr_type(y,0) in
																													 if check = false then raise Type_Error else
																													 	if (give_exp_type(y,new_scope)) <> 1 then raise Type_Error else
                                                      				(String.concat(["var " , x ," = " , expr_to_string(y,scope_map),";" , "\n"]) ,new_scope)
                                                    end

  | convert_statement(Absyn.assign(x,y),scope_map) = if MapT.find(scope_map,x) = NONE then raise scope_error else
																										 if give_expr_type(y,0) = false then raise Type_Error else
																										 		if SOME(give_exp_type(y,scope_map)) <> MapT.find(scope_map,x) then raise Assign_error
																												else
                                                        	(String.concat([x ," = " , expr_to_string(y,scope_map),";" ,"\n"]) , scope_map)

  | convert_statement(Absyn.ifelse(statms1,exp,statms2),scope_map) =
                let val e = expr_to_string(exp,scope_map)
                    val (s1,_) = convert_statements(statms1,"",scope_map)
                    val (s2,_) = convert_statements(statms2,"",scope_map) in
                    (String.concat(["if ( ",e ," )", "{" , "\n" , s1 , "}","\n", "else ","{" ,"\n",s2 , "}"]) ,scope_map)
                end

  | convert_statement(Absyn.loop(exp,statms),scope_map) = let val e = expr_to_string(exp,scope_map)
                                                    val (s,_) = convert_statements(statms,"",scope_map) in
                      (String.concat(["while","(", e,")","{","\n",s,"}","\n"]) ,scope_map)
                      end
  | convert_statement(Absyn.ifstat(statms,exp),scope_map) = let val e = expr_to_string(exp,scope_map)
                                                     val (s1,_) = convert_statements(statms,"",scope_map) in
                                      (String.concat(["if"," ( ",e," ) ", " { " ,"\n",s1,"}"]) ,scope_map)
                                  end
  | convert_statement(Absyn.Break,scope_map) = (String.concat(["break",";","\n"]) ,scope_map)
  | convert_statement(Absyn.Continue,scope_map) = (String.concat(["continue",";","\n"]) , scope_map)
  | convert_statement(Absyn.Return(exp),scope_map) = let val e = expr_to_string(exp,scope_map) in
																											(String.concat(["return","(",e,")",";","\n"]) , scope_map)
																										end
  | convert_statement(Absyn.Print(x),scope_map) = let val e = expr_to_string(x,scope_map) in
                                            (String.concat(["console.log(",e,")",";","\n"]) ,scope_map)
                                        end
  | convert_statement(Absyn.forloop(statm1,exp,statm2,statms),scope_map) = let val (s1,_) = convert_statement(statm1,scope_map)
                                                                  val e = expr_to_string(exp,scope_map)
                                                                  val (s2,_) = convert_statement(statm2,scope_map)
                                                                  val (s,_) = convert_statements(statms,"",scope_map) in
                                                                  (String.concat(["for ( ", s1,e,";",s2 ," )" , " {" ,"\n",s,"}","\n"]),scope_map)
                                                              end
  | convert_statement(Absyn.function_decl(Id,params_decl,statms),scope_map) = let fun list_to_string([],s) = ""
                                                                            | list_to_string(str::[],s) = str
                                                                            | list_to_string(p::params_decl,s) =
                                                                                                  let val s_new  = list_to_string(params_decl,s) in
                                                                                                      String.concat([p,",",s_new])
                                                                                                  end
                                                                    val p = list_to_string(params_decl,"")
                                                                    val new_scope = insert_list_in_map(params_decl,MapT.empty)
                                                                    val (s,_) = convert_statements(statms,"",new_scope) in
                            (String.concat(["function ",Id,"(",p," )"," {","\n",s,"}","\n"]) ,scope_map) end

  | convert_statement(Absyn.function_call(ret,Id,params),scope_map) = let val p = exprs_to_string(params,"",scope_map) in
                                                              (String.concat([ret," = ", Id," (",p,")",";","\n"]) ,scope_map)
                                                        end

	| convert_statement(Absyn.string_decl(Id,Absyn.Str(str)),scope_map) = let val new_scope = MapT.insert(scope_map,Id,0) in
																																	(String.concat(["var ",Id , " = ","'",str,"'",";","\n"]),new_scope)
																															end

	| convert_statement(Absyn.string_assign(Id,Absyn.Str(str)),scope_map) =  let val typ = MapT.find(scope_map,Id) in
																																if typ  = NONE then raise scope_error
																															 	else
																																		if typ = SOME(1) then raise Type_Error
																																		else
																																			(String.concat(["var ",Id , " = ","'",str,"'",";","\n"]),scope_map)
																																end
(*
We will have a clause for each constructor in our Statm datatype in abstract syntax structure declaration
*)

fun convert_to_js(absyn) = let  val (code_gen,_ )= convert_statements(absyn,"",MapT.empty) in
                                code_gen
                           end

(*
A wrapper function is what the purpose of the below function
*)

fun code_generator(filename,file_to_write) = let val absyn = Parse.parse(filename)
                                                 val file_out = TextIO.openOut file_to_write
                                                 val gen_code = convert_to_js(absyn)
                                                 fun write_output(t) = TextIO.output(file_out,t)
                                            in
                                                write_output(gen_code);
                                                TextIO.closeOut(file_out)

                                             end
end
