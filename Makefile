SOURCE_DIRS=programming-in-ml/standalone-program \
	  programming-in-ml/getting-started    \
	  reverse-polish


.PHONY: test clean

test:
	touch output.js
	sml cmd.sml
	nodejs output.js
	
clean:
	rm *.js
