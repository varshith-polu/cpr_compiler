# VR-Compiler #
# Designed By:- Polu Varshith(111501020) , Rahul Dhawan(111501023) #
# README #

This is a simple complier for C with some changes 

# characteristics  #
This doesn't have a main function like C you can go on writing like python but without indentation
Braces should be given for every construct so that works as the indentation in python

# constructs #
we have very few constructs like
if else and if (in conditionals)
while and for (in loops)
functions

# How to use them #
if (conditional expression) 
{
	your statements
};
Note: semi colon is there at the end)
Every statement ends with semi colon like c and also constructs like if else while for also end with semi colon

if (conditional expressions)
{

};
else
{


};


while(conditional expressional)
{
 your statements
};

Functional declarations

fun function_name(arguments){
	your statements
};

Functional calls
function_name(arguments)


# For Running the VR-Compiler #
test.c is input file name

output.js is output file name( it should have been created ).

1.) Compile the .lex and .grm file.

2.) In sml interpreter run command " CM.make "sources.cm"; ".

3.) In same run command " Code_gen.code_generator("test.c","output.js"); ".
