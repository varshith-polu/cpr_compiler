/* This is the another test file where  if-else or nested if-else */

int x =  5;
if ( x == 5 ) {
	x = 6;
};
else{
	x = 4;
};

if ( x == 6 ) {
	x = 7;
	if( x == 5 ){
		x = 8;
	};
};