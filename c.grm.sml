functor vrLrValsFun(structure Token : TOKEN)
 : sig structure ParserData : PARSER_DATA
       structure Tokens : vr_TOKENS
   end
 = 
struct
structure ParserData=
struct
structure Header = 
struct


structure A = Absyn

(*
A is the structure which contains abstract datatypes which makeup the Abstract syntax tree
*)

end
structure LrTable = Token.LrTable
structure Token = Token
local open LrTable in 
val table=let val actionRows =
"\
\\001\000\001\000\000\000\000\000\
\\001\000\002\000\028\000\003\000\027\000\004\000\026\000\005\000\025\000\
\\006\000\137\000\009\000\137\000\010\000\137\000\023\000\137\000\
\\033\000\137\000\000\000\
\\001\000\002\000\028\000\003\000\027\000\004\000\026\000\005\000\025\000\
\\006\000\138\000\009\000\138\000\010\000\138\000\023\000\138\000\
\\033\000\138\000\000\000\
\\001\000\002\000\028\000\003\000\027\000\004\000\026\000\005\000\025\000\
\\006\000\139\000\009\000\139\000\010\000\139\000\023\000\139\000\
\\033\000\139\000\000\000\
\\001\000\002\000\028\000\003\000\027\000\004\000\026\000\005\000\025\000\
\\006\000\140\000\009\000\140\000\010\000\140\000\023\000\140\000\
\\033\000\140\000\000\000\
\\001\000\002\000\028\000\003\000\027\000\004\000\026\000\005\000\025\000\
\\006\000\141\000\009\000\141\000\010\000\141\000\023\000\141\000\
\\033\000\141\000\000\000\
\\001\000\002\000\028\000\003\000\027\000\004\000\026\000\005\000\025\000\
\\006\000\024\000\009\000\086\000\010\000\023\000\011\000\022\000\
\\012\000\021\000\013\000\020\000\014\000\019\000\022\000\018\000\000\000\
\\001\000\002\000\028\000\003\000\027\000\004\000\026\000\005\000\025\000\
\\006\000\024\000\010\000\023\000\011\000\022\000\012\000\021\000\
\\013\000\020\000\014\000\019\000\022\000\018\000\023\000\068\000\000\000\
\\001\000\002\000\028\000\003\000\027\000\004\000\026\000\005\000\025\000\
\\006\000\024\000\010\000\023\000\011\000\022\000\012\000\021\000\
\\013\000\020\000\014\000\019\000\022\000\018\000\023\000\069\000\000\000\
\\001\000\002\000\028\000\003\000\027\000\004\000\026\000\005\000\025\000\
\\006\000\024\000\010\000\023\000\011\000\022\000\012\000\021\000\
\\013\000\020\000\014\000\019\000\022\000\018\000\023\000\071\000\000\000\
\\001\000\002\000\028\000\003\000\027\000\004\000\026\000\005\000\025\000\
\\006\000\024\000\010\000\023\000\011\000\022\000\012\000\021\000\
\\013\000\020\000\014\000\019\000\022\000\018\000\023\000\073\000\000\000\
\\001\000\007\000\017\000\008\000\016\000\017\000\015\000\025\000\014\000\
\\027\000\013\000\028\000\012\000\029\000\011\000\030\000\010\000\
\\031\000\009\000\032\000\008\000\034\000\007\000\035\000\006\000\000\000\
\\001\000\009\000\029\000\000\000\
\\001\000\009\000\067\000\000\000\
\\001\000\016\000\052\000\000\000\
\\001\000\016\000\061\000\000\000\
\\001\000\019\000\076\000\000\000\
\\001\000\019\000\081\000\000\000\
\\001\000\019\000\082\000\000\000\
\\001\000\019\000\083\000\000\000\
\\001\000\019\000\100\000\000\000\
\\001\000\019\000\101\000\000\000\
\\001\000\020\000\092\000\000\000\
\\001\000\020\000\095\000\000\000\
\\001\000\020\000\096\000\000\000\
\\001\000\020\000\097\000\000\000\
\\001\000\020\000\104\000\000\000\
\\001\000\020\000\105\000\000\000\
\\001\000\023\000\066\000\025\000\065\000\000\000\
\\001\000\023\000\074\000\000\000\
\\001\000\023\000\080\000\025\000\040\000\027\000\013\000\000\000\
\\001\000\023\000\087\000\000\000\
\\001\000\023\000\098\000\000\000\
\\001\000\024\000\032\000\000\000\
\\001\000\024\000\033\000\000\000\
\\001\000\024\000\034\000\000\000\
\\001\000\024\000\036\000\000\000\
\\001\000\024\000\038\000\000\000\
\\001\000\024\000\053\000\000\000\
\\001\000\025\000\030\000\000\000\
\\001\000\025\000\031\000\000\000\
\\001\000\025\000\037\000\000\000\
\\001\000\025\000\040\000\027\000\013\000\000\000\
\\001\000\025\000\059\000\026\000\058\000\027\000\013\000\000\000\
\\001\000\025\000\065\000\000\000\
\\001\000\026\000\063\000\000\000\
\\107\000\000\000\
\\108\000\000\000\
\\109\000\007\000\017\000\008\000\016\000\017\000\015\000\025\000\014\000\
\\027\000\013\000\028\000\012\000\029\000\011\000\030\000\010\000\
\\031\000\009\000\032\000\008\000\034\000\007\000\035\000\006\000\000\000\
\\110\000\000\000\
\\111\000\000\000\
\\112\000\002\000\028\000\003\000\027\000\004\000\026\000\005\000\025\000\
\\006\000\024\000\010\000\023\000\011\000\022\000\012\000\021\000\
\\013\000\020\000\014\000\019\000\022\000\018\000\000\000\
\\113\000\002\000\028\000\003\000\027\000\004\000\026\000\005\000\025\000\
\\006\000\024\000\010\000\023\000\011\000\022\000\012\000\021\000\
\\013\000\020\000\014\000\019\000\022\000\018\000\000\000\
\\114\000\002\000\028\000\003\000\027\000\004\000\026\000\005\000\025\000\
\\006\000\024\000\010\000\023\000\011\000\022\000\012\000\021\000\
\\013\000\020\000\014\000\019\000\022\000\018\000\000\000\
\\115\000\000\000\
\\116\000\000\000\
\\117\000\000\000\
\\118\000\018\000\099\000\000\000\
\\119\000\000\000\
\\120\000\000\000\
\\121\000\000\000\
\\122\000\000\000\
\\123\000\000\000\
\\124\000\002\000\028\000\003\000\027\000\004\000\026\000\005\000\025\000\
\\006\000\024\000\010\000\023\000\011\000\022\000\012\000\021\000\
\\013\000\020\000\014\000\019\000\022\000\018\000\033\000\088\000\000\000\
\\125\000\000\000\
\\126\000\000\000\
\\127\000\000\000\
\\128\000\033\000\075\000\000\000\
\\129\000\000\000\
\\130\000\000\000\
\\131\000\003\000\027\000\005\000\025\000\000\000\
\\132\000\000\000\
\\133\000\003\000\027\000\005\000\025\000\000\000\
\\134\000\000\000\
\\135\000\002\000\028\000\003\000\027\000\004\000\026\000\005\000\025\000\
\\011\000\022\000\012\000\021\000\013\000\020\000\014\000\019\000\
\\022\000\018\000\000\000\
\\136\000\002\000\028\000\003\000\027\000\004\000\026\000\005\000\025\000\
\\011\000\022\000\012\000\021\000\013\000\020\000\014\000\019\000\
\\022\000\018\000\000\000\
\\142\000\000\000\
\\142\000\016\000\035\000\000\000\
\\142\000\024\000\070\000\000\000\
\\143\000\000\000\
\"
val actionRowNumbers =
"\011\000\051\000\012\000\046\000\
\\039\000\040\000\033\000\034\000\
\\035\000\059\000\058\000\079\000\
\\077\000\036\000\041\000\037\000\
\\042\000\042\000\042\000\042\000\
\\042\000\042\000\042\000\042\000\
\\042\000\042\000\042\000\048\000\
\\014\000\038\000\011\000\042\000\
\\042\000\043\000\042\000\015\000\
\\042\000\005\000\076\000\002\000\
\\003\000\001\000\004\000\075\000\
\\074\000\073\000\072\000\071\000\
\\070\000\047\000\045\000\028\000\
\\013\000\007\000\008\000\053\000\
\\050\000\078\000\009\000\042\000\
\\010\000\049\000\029\000\067\000\
\\016\000\042\000\061\000\060\000\
\\030\000\017\000\052\000\018\000\
\\019\000\044\000\011\000\006\000\
\\031\000\063\000\065\000\011\000\
\\011\000\011\000\066\000\022\000\
\\011\000\064\000\042\000\023\000\
\\024\000\025\000\069\000\032\000\
\\062\000\057\000\054\000\068\000\
\\020\000\021\000\011\000\011\000\
\\026\000\027\000\055\000\056\000\
\\000\000"
val gotoT =
"\
\\001\000\104\000\002\000\003\000\003\000\002\000\004\000\001\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\004\000\037\000\000\000\
\\004\000\039\000\000\000\
\\004\000\040\000\000\000\
\\004\000\041\000\000\000\
\\004\000\042\000\000\000\
\\004\000\043\000\000\000\
\\004\000\044\000\000\000\
\\004\000\045\000\000\000\
\\004\000\046\000\000\000\
\\004\000\047\000\000\000\
\\004\000\048\000\000\000\
\\002\000\049\000\003\000\002\000\004\000\001\000\000\000\
\\000\000\
\\000\000\
\\003\000\052\000\004\000\001\000\000\000\
\\004\000\053\000\000\000\
\\004\000\054\000\000\000\
\\004\000\055\000\000\000\
\\004\000\058\000\000\000\
\\000\000\
\\004\000\060\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\006\000\062\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\004\000\070\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\004\000\075\000\000\000\
\\000\000\
\\000\000\
\\004\000\077\000\005\000\076\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\006\000\082\000\000\000\
\\002\000\083\000\003\000\002\000\004\000\001\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\002\000\087\000\003\000\002\000\004\000\001\000\000\000\
\\002\000\088\000\003\000\002\000\004\000\001\000\000\000\
\\002\000\089\000\003\000\002\000\004\000\001\000\000\000\
\\000\000\
\\000\000\
\\003\000\091\000\004\000\001\000\000\000\
\\000\000\
\\004\000\077\000\005\000\092\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\002\000\100\000\003\000\002\000\004\000\001\000\000\000\
\\002\000\101\000\003\000\002\000\004\000\001\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\"
val numstates = 105
val numrules = 37
val s = ref "" and index = ref 0
val string_to_int = fn () => 
let val i = !index
in index := i+2; Char.ord(String.sub(!s,i)) + Char.ord(String.sub(!s,i+1)) * 256
end
val string_to_list = fn s' =>
    let val len = String.size s'
        fun f () =
           if !index < len then string_to_int() :: f()
           else nil
   in index := 0; s := s'; f ()
   end
val string_to_pairlist = fn (conv_key,conv_entry) =>
     let fun f () =
         case string_to_int()
         of 0 => EMPTY
          | n => PAIR(conv_key (n-1),conv_entry (string_to_int()),f())
     in f
     end
val string_to_pairlist_default = fn (conv_key,conv_entry) =>
    let val conv_row = string_to_pairlist(conv_key,conv_entry)
    in fn () =>
       let val default = conv_entry(string_to_int())
           val row = conv_row()
       in (row,default)
       end
   end
val string_to_table = fn (convert_row,s') =>
    let val len = String.size s'
        fun f ()=
           if !index < len then convert_row() :: f()
           else nil
     in (s := s'; index := 0; f ())
     end
local
  val memo = Array.array(numstates+numrules,ERROR)
  val _ =let fun g i=(Array.update(memo,i,REDUCE(i-numstates)); g(i+1))
       fun f i =
            if i=numstates then g i
            else (Array.update(memo,i,SHIFT (STATE i)); f (i+1))
          in f 0 handle General.Subscript => ()
          end
in
val entry_to_action = fn 0 => ACCEPT | 1 => ERROR | j => Array.sub(memo,(j-2))
end
val gotoT=Array.fromList(string_to_table(string_to_pairlist(NT,STATE),gotoT))
val actionRows=string_to_table(string_to_pairlist_default(T,entry_to_action),actionRows)
val actionRowNumbers = string_to_list actionRowNumbers
val actionT = let val actionRowLookUp=
let val a=Array.fromList(actionRows) in fn i=>Array.sub(a,i) end
in Array.fromList(List.map actionRowLookUp actionRowNumbers)
end
in LrTable.mkLrTable {actions=actionT,gotos=gotoT,numRules=numrules,
numStates=numstates,initialState=STATE 0}
end
end
local open Header in
type pos = int
type arg = unit
structure MlyValue = 
struct
datatype svalue = VOID | ntVOID of unit ->  unit
 | INT of unit ->  (int) | STRING of unit ->  (string)
 | ID of unit ->  (string) | params_decl of unit ->  (A.Params_decl)
 | params of unit ->  (A.Params) | exp of unit ->  (A.Expr)
 | statm of unit ->  (A.Statm) | statms of unit ->  (A.Statm list)
 | program of unit ->  (A.Statm list)
end
type svalue = MlyValue.svalue
type result = A.Statm list
end
structure EC=
struct
open LrTable
infix 5 $$
fun x $$ y = y::x
val is_keyword =
fn _ => false
val preferred_change : (term list * term list) list = 
nil
val noShift = 
fn (T 0) => true | _ => false
val showTerminal =
fn (T 0) => "EOF"
  | (T 1) => "PLUS"
  | (T 2) => "TIMES"
  | (T 3) => "MINUS"
  | (T 4) => "DIVIDE"
  | (T 5) => "AND"
  | (T 6) => "WHILE"
  | (T 7) => "INT_TYPE"
  | (T 8) => "SEMI"
  | (T 9) => "OR"
  | (T 10) => "LT"
  | (T 11) => "GE"
  | (T 12) => "GT"
  | (T 13) => "LE"
  | (T 14) => "NIL"
  | (T 15) => "ASSIGN"
  | (T 16) => "IF"
  | (T 17) => "ELSE"
  | (T 18) => "LBRACE"
  | (T 19) => "RBRACE"
  | (T 20) => "NEQ"
  | (T 21) => "EQ"
  | (T 22) => "RPAREN"
  | (T 23) => "LPAREN"
  | (T 24) => "ID"
  | (T 25) => "STRING"
  | (T 26) => "INT"
  | (T 27) => "BREAK"
  | (T 28) => "CONTINUE"
  | (T 29) => "RETURN"
  | (T 30) => "PRINT"
  | (T 31) => "FOR"
  | (T 32) => "COMMA"
  | (T 33) => "FUN"
  | (T 34) => "STRING_TYPE"
  | _ => "bogus-term"
local open Header in
val errtermvalue=
fn (T 24) => MlyValue.ID(fn () => ("cpr")) | 
(T 26) => MlyValue.INT(fn () => (1)) | 
(T 25) => MlyValue.STRING(fn () => ("")) | 
_ => MlyValue.VOID
end
val terms : term list = nil
 $$ (T 34) $$ (T 33) $$ (T 32) $$ (T 31) $$ (T 30) $$ (T 29) $$ (T 28)
 $$ (T 27) $$ (T 23) $$ (T 22) $$ (T 21) $$ (T 20) $$ (T 19) $$ (T 18)
 $$ (T 17) $$ (T 16) $$ (T 15) $$ (T 14) $$ (T 13) $$ (T 12) $$ (T 11)
 $$ (T 10) $$ (T 9) $$ (T 8) $$ (T 7) $$ (T 6) $$ (T 5) $$ (T 4) $$ 
(T 3) $$ (T 2) $$ (T 1) $$ (T 0)end
structure Actions =
struct 
exception mlyAction of int
local open Header in
val actions = 
fn (i392,defaultPos,stack,
    (()):arg) =>
case (i392,stack)
of  ( 0, ( ( _, ( MlyValue.statms statms1, statms1left, statms1right))
 :: rest671)) => let val  result = MlyValue.program (fn _ => let val 
 (statms as statms1) = statms1 ()
 in (statms)
end)
 in ( LrTable.NT 0, ( result, statms1left, statms1right), rest671)
end
|  ( 1, ( ( _, ( MlyValue.statms statms1, _, statms1right)) :: _ :: (
 _, ( MlyValue.statm statm1, statm1left, _)) :: rest671)) => let val  
result = MlyValue.statms (fn _ => let val  (statm as statm1) = statm1
 ()
 val  (statms as statms1) = statms1 ()
 in (statm::statms)
end)
 in ( LrTable.NT 1, ( result, statm1left, statms1right), rest671)
end
|  ( 2, ( ( _, ( _, _, SEMI1right)) :: ( _, ( MlyValue.statm statm1, 
statm1left, _)) :: rest671)) => let val  result = MlyValue.statms (fn
 _ => let val  (statm as statm1) = statm1 ()
 in ([statm])
end)
 in ( LrTable.NT 1, ( result, statm1left, SEMI1right), rest671)
end
|  ( 3, ( ( _, ( MlyValue.STRING STRING1, _, STRING1right)) :: _ :: (
 _, ( MlyValue.ID ID1, _, _)) :: ( _, ( _, STRING_TYPE1left, _)) :: 
rest671)) => let val  result = MlyValue.statm (fn _ => let val  (ID
 as ID1) = ID1 ()
 val  (STRING as STRING1) = STRING1 ()
 in (A.string_decl(ID,A.Str(STRING)))
end)
 in ( LrTable.NT 2, ( result, STRING_TYPE1left, STRING1right), rest671
)
end
|  ( 4, ( ( _, ( MlyValue.STRING STRING1, _, STRING1right)) :: _ :: (
 _, ( MlyValue.ID ID1, ID1left, _)) :: rest671)) => let val  result = 
MlyValue.statm (fn _ => let val  (ID as ID1) = ID1 ()
 val  (STRING as STRING1) = STRING1 ()
 in (A.string_assign(ID,A.Str(STRING)))
end)
 in ( LrTable.NT 2, ( result, ID1left, STRING1right), rest671)
end
|  ( 5, ( ( _, ( MlyValue.exp exp1, exp1left, exp1right)) :: rest671))
 => let val  result = MlyValue.statm (fn _ => let val  (exp as exp1) =
 exp1 ()
 in (A.ab_exp(exp))
end)
 in ( LrTable.NT 2, ( result, exp1left, exp1right), rest671)
end
|  ( 6, ( ( _, ( MlyValue.exp exp1, _, exp1right)) :: _ :: ( _, ( 
MlyValue.ID ID1, _, _)) :: ( _, ( _, INT_TYPE1left, _)) :: rest671))
 => let val  result = MlyValue.statm (fn _ => let val  (ID as ID1) = 
ID1 ()
 val  (exp as exp1) = exp1 ()
 in (A.decl(ID,exp))
end)
 in ( LrTable.NT 2, ( result, INT_TYPE1left, exp1right), rest671)
end
|  ( 7, ( ( _, ( MlyValue.exp exp1, _, exp1right)) :: _ :: ( _, ( 
MlyValue.ID ID1, ID1left, _)) :: rest671)) => let val  result = 
MlyValue.statm (fn _ => let val  (ID as ID1) = ID1 ()
 val  (exp as exp1) = exp1 ()
 in (A.assign(ID,exp))
end)
 in ( LrTable.NT 2, ( result, ID1left, exp1right), rest671)
end
|  ( 8, ( ( _, ( _, _, RBRACE1right)) :: ( _, ( MlyValue.statms 
statms1, _, _)) :: _ :: _ :: ( _, ( MlyValue.exp exp1, _, _)) :: _ :: 
( _, ( _, WHILE1left, _)) :: rest671)) => let val  result = 
MlyValue.statm (fn _ => let val  (exp as exp1) = exp1 ()
 val  (statms as statms1) = statms1 ()
 in (A.loop(exp,statms))
end)
 in ( LrTable.NT 2, ( result, WHILE1left, RBRACE1right), rest671)
end
|  ( 9, ( ( _, ( _, _, RBRACE1right)) :: ( _, ( MlyValue.statms 
statms1, _, _)) :: _ :: _ :: ( _, ( MlyValue.statm statm2, _, _)) :: _
 :: ( _, ( MlyValue.exp exp1, _, _)) :: _ :: ( _, ( MlyValue.statm 
statm1, _, _)) :: _ :: ( _, ( _, FOR1left, _)) :: rest671)) => let
 val  result = MlyValue.statm (fn _ => let val  statm1 = statm1 ()
 val  (exp as exp1) = exp1 ()
 val  statm2 = statm2 ()
 val  (statms as statms1) = statms1 ()
 in (A.forloop(statm1,exp,statm2,statms))
end)
 in ( LrTable.NT 2, ( result, FOR1left, RBRACE1right), rest671)
end
|  ( 10, ( ( _, ( _, _, RBRACE2right)) :: ( _, ( MlyValue.statms 
statms2, _, _)) :: _ :: _ :: _ :: ( _, ( MlyValue.statms statms1, _, _
)) :: _ :: _ :: ( _, ( MlyValue.exp exp1, _, _)) :: _ :: ( _, ( _, 
IF1left, _)) :: rest671)) => let val  result = MlyValue.statm (fn _ =>
 let val  (exp as exp1) = exp1 ()
 val  statms1 = statms1 ()
 val  statms2 = statms2 ()
 in (A.ifelse(statms1,exp,statms2))
end)
 in ( LrTable.NT 2, ( result, IF1left, RBRACE2right), rest671)
end
|  ( 11, ( ( _, ( _, _, RBRACE1right)) :: ( _, ( MlyValue.statms 
statms1, _, _)) :: _ :: _ :: ( _, ( MlyValue.exp exp1, _, _)) :: _ :: 
( _, ( _, IF1left, _)) :: rest671)) => let val  result = 
MlyValue.statm (fn _ => let val  (exp as exp1) = exp1 ()
 val  statms1 = statms1 ()
 in (A.ifstat(statms1,exp))
end)
 in ( LrTable.NT 2, ( result, IF1left, RBRACE1right), rest671)
end
|  ( 12, ( ( _, ( _, BREAK1left, BREAK1right)) :: rest671)) => let
 val  result = MlyValue.statm (fn _ => (A.Break))
 in ( LrTable.NT 2, ( result, BREAK1left, BREAK1right), rest671)
end
|  ( 13, ( ( _, ( _, CONTINUE1left, CONTINUE1right)) :: rest671)) =>
 let val  result = MlyValue.statm (fn _ => (A.Continue))
 in ( LrTable.NT 2, ( result, CONTINUE1left, CONTINUE1right), rest671)

end
|  ( 14, ( ( _, ( _, _, RPAREN1right)) :: ( _, ( MlyValue.exp exp1, _,
 _)) :: _ :: ( _, ( _, RETURN1left, _)) :: rest671)) => let val  
result = MlyValue.statm (fn _ => let val  (exp as exp1) = exp1 ()
 in (A.Return(exp))
end)
 in ( LrTable.NT 2, ( result, RETURN1left, RPAREN1right), rest671)
end
|  ( 15, ( ( _, ( _, _, RPAREN1right)) :: ( _, ( MlyValue.exp exp1, _,
 _)) :: _ :: ( _, ( _, PRINT1left, _)) :: rest671)) => let val  result
 = MlyValue.statm (fn _ => let val  (exp as exp1) = exp1 ()
 in (A.Print(exp))
end)
 in ( LrTable.NT 2, ( result, PRINT1left, RPAREN1right), rest671)
end
|  ( 16, ( ( _, ( MlyValue.params params1, _, params1right)) :: _ :: (
 _, ( MlyValue.exp exp1, exp1left, _)) :: rest671)) => let val  result
 = MlyValue.params (fn _ => let val  (exp as exp1) = exp1 ()
 val  (params as params1) = params1 ()
 in (exp::params)
end)
 in ( LrTable.NT 4, ( result, exp1left, params1right), rest671)
end
|  ( 17, ( ( _, ( MlyValue.exp exp1, exp1left, exp1right)) :: rest671)
) => let val  result = MlyValue.params (fn _ => let val  (exp as exp1)
 = exp1 ()
 in ([exp])
end)
 in ( LrTable.NT 4, ( result, exp1left, exp1right), rest671)
end
|  ( 18, ( ( _, ( _, _, RPAREN1right)) :: ( _, ( MlyValue.params 
params1, _, _)) :: _ :: ( _, ( MlyValue.ID ID2, _, _)) :: _ :: ( _, ( 
MlyValue.ID ID1, ID1left, _)) :: rest671)) => let val  result = 
MlyValue.statm (fn _ => let val  ID1 = ID1 ()
 val  ID2 = ID2 ()
 val  (params as params1) = params1 ()
 in (A.function_call(ID1,ID2,params))
end)
 in ( LrTable.NT 2, ( result, ID1left, RPAREN1right), rest671)
end
|  ( 19, ( ( _, ( _, _, RPAREN1right)) :: _ :: ( _, ( MlyValue.ID ID2,
 _, _)) :: _ :: ( _, ( MlyValue.ID ID1, ID1left, _)) :: rest671)) =>
 let val  result = MlyValue.statm (fn _ => let val  ID1 = ID1 ()
 val  ID2 = ID2 ()
 in (A.function_call(ID1,ID2,[]))
end)
 in ( LrTable.NT 2, ( result, ID1left, RPAREN1right), rest671)
end
|  ( 20, ( ( _, ( MlyValue.params_decl params_decl1, _, 
params_decl1right)) :: _ :: ( _, ( MlyValue.ID ID1, ID1left, _)) :: 
rest671)) => let val  result = MlyValue.params_decl (fn _ => let val 
 (ID as ID1) = ID1 ()
 val  (params_decl as params_decl1) = params_decl1 ()
 in (ID :: params_decl)
end)
 in ( LrTable.NT 5, ( result, ID1left, params_decl1right), rest671)

end
|  ( 21, ( ( _, ( MlyValue.ID ID1, ID1left, ID1right)) :: rest671)) =>
 let val  result = MlyValue.params_decl (fn _ => let val  (ID as ID1)
 = ID1 ()
 in ([ID])
end)
 in ( LrTable.NT 5, ( result, ID1left, ID1right), rest671)
end
|  ( 22, ( ( _, ( _, _, RBRACE1right)) :: ( _, ( MlyValue.statms 
statms1, _, _)) :: _ :: _ :: ( _, ( MlyValue.params_decl params_decl1,
 _, _)) :: _ :: ( _, ( MlyValue.ID ID1, _, _)) :: ( _, ( _, FUN1left,
 _)) :: rest671)) => let val  result = MlyValue.statm (fn _ => let
 val  (ID as ID1) = ID1 ()
 val  (params_decl as params_decl1) = params_decl1 ()
 val  (statms as statms1) = statms1 ()
 in (A.function_decl(ID,params_decl,statms))
end)
 in ( LrTable.NT 2, ( result, FUN1left, RBRACE1right), rest671)
end
|  ( 23, ( ( _, ( _, _, RBRACE1right)) :: ( _, ( MlyValue.statms 
statms1, _, _)) :: _ :: _ :: _ :: ( _, ( MlyValue.ID ID1, _, _)) :: (
 _, ( _, FUN1left, _)) :: rest671)) => let val  result = 
MlyValue.statm (fn _ => let val  (ID as ID1) = ID1 ()
 val  (statms as statms1) = statms1 ()
 in (A.function_decl(ID,[],statms))
end)
 in ( LrTable.NT 2, ( result, FUN1left, RBRACE1right), rest671)
end
|  ( 24, ( ( _, ( MlyValue.exp exp2, _, exp2right)) :: _ :: ( _, ( 
MlyValue.exp exp1, exp1left, _)) :: rest671)) => let val  result = 
MlyValue.exp (fn _ => let val  exp1 = exp1 ()
 val  exp2 = exp2 ()
 in (A.plus exp1 exp2)
end)
 in ( LrTable.NT 3, ( result, exp1left, exp2right), rest671)
end
|  ( 25, ( ( _, ( MlyValue.exp exp2, _, exp2right)) :: _ :: ( _, ( 
MlyValue.exp exp1, exp1left, _)) :: rest671)) => let val  result = 
MlyValue.exp (fn _ => let val  exp1 = exp1 ()
 val  exp2 = exp2 ()
 in (A.times exp1 exp2)
end)
 in ( LrTable.NT 3, ( result, exp1left, exp2right), rest671)
end
|  ( 26, ( ( _, ( MlyValue.exp exp2, _, exp2right)) :: _ :: ( _, ( 
MlyValue.exp exp1, exp1left, _)) :: rest671)) => let val  result = 
MlyValue.exp (fn _ => let val  exp1 = exp1 ()
 val  exp2 = exp2 ()
 in (A.minus exp1 exp2)
end)
 in ( LrTable.NT 3, ( result, exp1left, exp2right), rest671)
end
|  ( 27, ( ( _, ( MlyValue.exp exp2, _, exp2right)) :: _ :: ( _, ( 
MlyValue.exp exp1, exp1left, _)) :: rest671)) => let val  result = 
MlyValue.exp (fn _ => let val  exp1 = exp1 ()
 val  exp2 = exp2 ()
 in (A.divide exp1 exp2)
end)
 in ( LrTable.NT 3, ( result, exp1left, exp2right), rest671)
end
|  ( 28, ( ( _, ( MlyValue.exp exp2, _, exp2right)) :: _ :: ( _, ( 
MlyValue.exp exp1, exp1left, _)) :: rest671)) => let val  result = 
MlyValue.exp (fn _ => let val  exp1 = exp1 ()
 val  exp2 = exp2 ()
 in (A.and_op exp1 exp2)
end)
 in ( LrTable.NT 3, ( result, exp1left, exp2right), rest671)
end
|  ( 29, ( ( _, ( MlyValue.exp exp2, _, exp2right)) :: _ :: ( _, ( 
MlyValue.exp exp1, exp1left, _)) :: rest671)) => let val  result = 
MlyValue.exp (fn _ => let val  exp1 = exp1 ()
 val  exp2 = exp2 ()
 in (A.or_op exp1 exp2)
end)
 in ( LrTable.NT 3, ( result, exp1left, exp2right), rest671)
end
|  ( 30, ( ( _, ( MlyValue.exp exp2, _, exp2right)) :: _ :: ( _, ( 
MlyValue.exp exp1, exp1left, _)) :: rest671)) => let val  result = 
MlyValue.exp (fn _ => let val  exp1 = exp1 ()
 val  exp2 = exp2 ()
 in (A.ge exp1 exp2)
end)
 in ( LrTable.NT 3, ( result, exp1left, exp2right), rest671)
end
|  ( 31, ( ( _, ( MlyValue.exp exp2, _, exp2right)) :: _ :: ( _, ( 
MlyValue.exp exp1, exp1left, _)) :: rest671)) => let val  result = 
MlyValue.exp (fn _ => let val  exp1 = exp1 ()
 val  exp2 = exp2 ()
 in (A.le exp1 exp2)
end)
 in ( LrTable.NT 3, ( result, exp1left, exp2right), rest671)
end
|  ( 32, ( ( _, ( MlyValue.exp exp2, _, exp2right)) :: _ :: ( _, ( 
MlyValue.exp exp1, exp1left, _)) :: rest671)) => let val  result = 
MlyValue.exp (fn _ => let val  exp1 = exp1 ()
 val  exp2 = exp2 ()
 in (A.gt exp1 exp2)
end)
 in ( LrTable.NT 3, ( result, exp1left, exp2right), rest671)
end
|  ( 33, ( ( _, ( MlyValue.exp exp2, _, exp2right)) :: _ :: ( _, ( 
MlyValue.exp exp1, exp1left, _)) :: rest671)) => let val  result = 
MlyValue.exp (fn _ => let val  exp1 = exp1 ()
 val  exp2 = exp2 ()
 in (A.lt exp1 exp2)
end)
 in ( LrTable.NT 3, ( result, exp1left, exp2right), rest671)
end
|  ( 34, ( ( _, ( MlyValue.exp exp2, _, exp2right)) :: _ :: ( _, ( 
MlyValue.exp exp1, exp1left, _)) :: rest671)) => let val  result = 
MlyValue.exp (fn _ => let val  exp1 = exp1 ()
 val  exp2 = exp2 ()
 in (A.eq exp1 exp2)
end)
 in ( LrTable.NT 3, ( result, exp1left, exp2right), rest671)
end
|  ( 35, ( ( _, ( MlyValue.ID ID1, ID1left, ID1right)) :: rest671)) =>
 let val  result = MlyValue.exp (fn _ => let val  (ID as ID1) = ID1 ()
 in (A.ID_abs(ID))
end)
 in ( LrTable.NT 3, ( result, ID1left, ID1right), rest671)
end
|  ( 36, ( ( _, ( MlyValue.INT INT1, INT1left, INT1right)) :: rest671)
) => let val  result = MlyValue.exp (fn _ => let val  (INT as INT1) = 
INT1 ()
 in (A.Const(INT))
end)
 in ( LrTable.NT 3, ( result, INT1left, INT1right), rest671)
end
| _ => raise (mlyAction i392)
end
val void = MlyValue.VOID
val extract = fn a => (fn MlyValue.program x => x
| _ => let exception ParseInternal
	in raise ParseInternal end) a ()
end
end
structure Tokens : vr_TOKENS =
struct
type svalue = ParserData.svalue
type ('a,'b) token = ('a,'b) Token.token
fun EOF (p1,p2) = Token.TOKEN (ParserData.LrTable.T 0,(
ParserData.MlyValue.VOID,p1,p2))
fun PLUS (p1,p2) = Token.TOKEN (ParserData.LrTable.T 1,(
ParserData.MlyValue.VOID,p1,p2))
fun TIMES (p1,p2) = Token.TOKEN (ParserData.LrTable.T 2,(
ParserData.MlyValue.VOID,p1,p2))
fun MINUS (p1,p2) = Token.TOKEN (ParserData.LrTable.T 3,(
ParserData.MlyValue.VOID,p1,p2))
fun DIVIDE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 4,(
ParserData.MlyValue.VOID,p1,p2))
fun AND (p1,p2) = Token.TOKEN (ParserData.LrTable.T 5,(
ParserData.MlyValue.VOID,p1,p2))
fun WHILE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 6,(
ParserData.MlyValue.VOID,p1,p2))
fun INT_TYPE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 7,(
ParserData.MlyValue.VOID,p1,p2))
fun SEMI (p1,p2) = Token.TOKEN (ParserData.LrTable.T 8,(
ParserData.MlyValue.VOID,p1,p2))
fun OR (p1,p2) = Token.TOKEN (ParserData.LrTable.T 9,(
ParserData.MlyValue.VOID,p1,p2))
fun LT (p1,p2) = Token.TOKEN (ParserData.LrTable.T 10,(
ParserData.MlyValue.VOID,p1,p2))
fun GE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 11,(
ParserData.MlyValue.VOID,p1,p2))
fun GT (p1,p2) = Token.TOKEN (ParserData.LrTable.T 12,(
ParserData.MlyValue.VOID,p1,p2))
fun LE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 13,(
ParserData.MlyValue.VOID,p1,p2))
fun NIL (p1,p2) = Token.TOKEN (ParserData.LrTable.T 14,(
ParserData.MlyValue.VOID,p1,p2))
fun ASSIGN (p1,p2) = Token.TOKEN (ParserData.LrTable.T 15,(
ParserData.MlyValue.VOID,p1,p2))
fun IF (p1,p2) = Token.TOKEN (ParserData.LrTable.T 16,(
ParserData.MlyValue.VOID,p1,p2))
fun ELSE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 17,(
ParserData.MlyValue.VOID,p1,p2))
fun LBRACE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 18,(
ParserData.MlyValue.VOID,p1,p2))
fun RBRACE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 19,(
ParserData.MlyValue.VOID,p1,p2))
fun NEQ (p1,p2) = Token.TOKEN (ParserData.LrTable.T 20,(
ParserData.MlyValue.VOID,p1,p2))
fun EQ (p1,p2) = Token.TOKEN (ParserData.LrTable.T 21,(
ParserData.MlyValue.VOID,p1,p2))
fun RPAREN (p1,p2) = Token.TOKEN (ParserData.LrTable.T 22,(
ParserData.MlyValue.VOID,p1,p2))
fun LPAREN (p1,p2) = Token.TOKEN (ParserData.LrTable.T 23,(
ParserData.MlyValue.VOID,p1,p2))
fun ID (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 24,(
ParserData.MlyValue.ID (fn () => i),p1,p2))
fun STRING (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 25,(
ParserData.MlyValue.STRING (fn () => i),p1,p2))
fun INT (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 26,(
ParserData.MlyValue.INT (fn () => i),p1,p2))
fun BREAK (p1,p2) = Token.TOKEN (ParserData.LrTable.T 27,(
ParserData.MlyValue.VOID,p1,p2))
fun CONTINUE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 28,(
ParserData.MlyValue.VOID,p1,p2))
fun RETURN (p1,p2) = Token.TOKEN (ParserData.LrTable.T 29,(
ParserData.MlyValue.VOID,p1,p2))
fun PRINT (p1,p2) = Token.TOKEN (ParserData.LrTable.T 30,(
ParserData.MlyValue.VOID,p1,p2))
fun FOR (p1,p2) = Token.TOKEN (ParserData.LrTable.T 31,(
ParserData.MlyValue.VOID,p1,p2))
fun COMMA (p1,p2) = Token.TOKEN (ParserData.LrTable.T 32,(
ParserData.MlyValue.VOID,p1,p2))
fun FUN (p1,p2) = Token.TOKEN (ParserData.LrTable.T 33,(
ParserData.MlyValue.VOID,p1,p2))
fun STRING_TYPE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 34,(
ParserData.MlyValue.VOID,p1,p2))
end
end
