(* ml declerations *)
(* must specify the lexresult type and eof fun *)
(* as we are including parser we donot need Tokes.sml parser will manage *)
type svalue = Tokens.svalue     
type pos = int     (* defining the type of pos *)
type ('a,'b) token = ('a,'b) Tokens.token   (* defining the type of the token *)
type lexresult = (svalue,pos) token   (* defining the type of lex-result *)


val lineNum = ErrorMsg.lineNum
val linePos = ErrorMsg.linePos
fun err(p1,p2) = ErrorMsg.error p1


fun eof() = let val pos = hd(!linePos) in Tokens.EOF(pos,pos) end
 (* this is also the must specifying thing that what to do at eof call just eof in tokens *)

exception NotAnInt

fun getInt(optionInt : int option) = case optionInt of
  SOME(n) => n
| _ => raise NotAnInt 

fun string_without_quotes(s:string) = String.extract (s,1,SOME(String.size s -2)) 


%%
%header (functor vrLexFun(structure Tokens: vr_TOKENS));

%s COMMENT;   
wordss = [a-zA-Z]+;		
digit = [0-9]+;		
id = {wordss}({wordss}|{digit}|_)*; 
space = [\ \t];
quote = ["];
notquote = [^"];

%%
<INITIAL>"=" => (Tokens.ASSIGN(yypos, yypos + size yytext));
<INITIAL>"if"  => (Tokens.IF(yypos,yypos + size yytext));
<INITIAL>"for"  => (Tokens.FOR(yypos,yypos + size yytext));
<INITIAL>"break"  => (Tokens.BREAK(yypos,yypos + size yytext));
<INITIAL>"continue"  => (Tokens.CONTINUE(yypos,yypos + size yytext));
<INITIAL>"return"  => (Tokens.RETURN(yypos,yypos + size yytext));
<INITIAL>"print"  => (Tokens.PRINT(yypos,yypos + size yytext));
<INITIAL>"int"  => (Tokens.INT_TYPE(yypos,yypos + size yytext));
<INITIAL>"string"  => (Tokens.STRING_TYPE(yypos,yypos + size yytext));
<INITIAL>\"{wordss}\"  => (Tokens.STRING(string_without_quotes(yytext), yypos,yypos + size yytext));
<INITIAL>"while" => (Tokens.WHILE(yypos,yypos + size yytext));
<INITIAL>"else" => (Tokens.ELSE(yypos,yypos + size yytext));
<INITIAL>"nil" => (Tokens.NIL(yypos,yypos + size yytext));
<INITIAL>"fun" => (Tokens.FUN(yypos,yypos + size yytext));
<INITIAL>"+"   => (Tokens.PLUS(yypos,yypos + size yytext));
<INITIAL>"-" => (Tokens.MINUS(yypos,yypos + size yytext));
<INITIAL>"*" => (Tokens.TIMES(yypos,yypos + size yytext));
<INITIAL>";" => (Tokens.SEMI(yypos,yypos + size yytext));
<INITIAL>"," => (Tokens.COMMA(yypos,yypos + size yytext));
<INITIAL>"/" => (Tokens.DIVIDE(yypos,yypos + size yytext));
<INITIAL>"(" => (Tokens.LPAREN(yypos,yypos + size yytext));
<INITIAL>")" => (Tokens.RPAREN(yypos,yypos + size yytext));
<INITIAL>"{" => (Tokens.LBRACE(yypos,yypos + size yytext));
<INITIAL>"}" => (Tokens.RBRACE(yypos,yypos + size yytext));
<INITIAL>"|"  => (Tokens.OR(yypos,yypos + size yytext));
<INITIAL>"&" => (Tokens.AND(yypos,yypos + size yytext));
<INITIAL>">=" => (Tokens.GE(yypos,yypos + size yytext));
<INITIAL>">"   => (Tokens.GT(yypos,yypos + size yytext));
<INITIAL>"<=" => (Tokens.LE(yypos,yypos + size yytext));
<INITIAL>"<" => (Tokens.LT(yypos,yypos + size yytext));
<INITIAL>"!=" => (Tokens.NEQ(yypos,yypos + size yytext));
<INITIAL>"==" => (Tokens.EQ(yypos,yypos + size yytext));
<INITIAL>{id} => (Tokens.ID(yytext,yypos,yypos + size yytext));
<INITIAL>{digit}+ => (Tokens.INT(getInt (Int.fromString yytext),yypos,yypos + size yytext));

<INITIAL>"/*" => (YYBEGIN COMMENT; continue());
<COMMENT>"*/" => (YYBEGIN INITIAL; continue());
<COMMENT>. => (continue());

{space}+ => (continue());
"\n"     => (lineNum := !lineNum+1; linePos := yypos :: !linePos; continue());
.        => (ErrorMsg.error yypos ("illegal character '" ^ yytext ^ "'"); continue());
